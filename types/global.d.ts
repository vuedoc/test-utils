import { ParsingResult } from '@vuedoc/parser';

interface VuedocMatchers<R = unknown> {
  toParseAs(expectedResult: ParsingResult, options?: ToParseAsOptions): R;
  toParseWithError(expectedErrorMessage: string, options?: ToParseAsOptions): R;
}

type ToParseAsOptions = {
  /**
   * By default `globalThis.VUEDOC_FAKE_NODEMODULES_PATHS`
   */
  fakeNodeModulesPaths: string[];
};

declare global {
  namespace jest {
    interface Expect extends VuedocMatchers {}
    interface Matchers<R> extends VuedocMatchers<R> {}
    interface InverseAsymmetricMatchers extends VuedocMatchers {}
  }
}

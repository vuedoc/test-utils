import { expect } from 'vitest';
import { merge } from '@b613/utils/lib/object.js';
import type { ParsingOptions } from '@vuedoc/parser';

// eslint-disable-next-line @typescript-eslint/no-namespace
declare namespace Intl {
  class ListFormat {
    constructor(locales?: string | string[], options?: any);
    public format: (items: string[]) => string;
  }
}

const formatter = new Intl.ListFormat('en', { style: 'long', type: 'conjunction' });

async function parseComponent(parsingOptions: ParsingOptions, fakeNodeModulesPaths: string) {
  if (fakeNodeModulesPaths) {
    if (!parsingOptions.resolver) {
      parsingOptions.resolver = {};
    }

    merge(parsingOptions.resolver, {
      paths: fakeNodeModulesPaths,
    });
  }

  const vuedoc: typeof import('@vuedoc/parser') = globalThis.VUEDOC_PACKAGE || (await import('@vuedoc/parser'));

  return vuedoc.parseComponent(parsingOptions);
}

expect.extend({
  async toParseAs(parsingOptions, expected, { fakeNodeModulesPaths = globalThis.VUEDOC_FAKE_NODEMODULES_PATHS } = {}) {
    const component = await parseComponent(parsingOptions, fakeNodeModulesPaths);
    const result = {
      paths: [],
      actual: {},
      expected: {},
    };

    for (const key in expected) {
      const actual = component[key];
      const pass = this.equals(actual, expected[key]);
      const hasError = this.isNot ? pass === false : !pass;

      if (hasError) {
        result.paths.push(`.${key}`);
        Object.assign(result.actual, { [key]: actual });
        Object.assign(result.expected, { [key]: expected[key] });
      }
    }

    return {
      pass: result.paths.length === 0,
      message: () => `The parsing result fields ${formatter.format(result.paths)} ${result.paths.length > 1 ? 'are' : 'is'}${this.isNot ? '' : ' not'} equal to the ${this.isNot ? 'unexpected' : 'expected'} result.`,
      actual: result.actual,
      expected: result.expected,
    };
  },
  async toParseWithError(parsingOptions, expected, { fakeNodeModulesPaths = globalThis.VUEDOC_FAKE_NODEMODULES_PATHS } = {}) {
    let errorMessage = null;

    try {
      const result = await parseComponent(parsingOptions, fakeNodeModulesPaths);

      errorMessage = result.errors[0];
    } catch (err) {
      errorMessage = err.message;
    }

    return {
      pass: errorMessage === expected,
      message: () => `The parsing should${this.isNot ? ' not' : ''} throw error.`,
      actual: errorMessage,
      expected: expected,
    };
  },
});

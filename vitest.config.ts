// eslint-disable-next-line import/extensions
import { defineConfig } from 'vitest/config';

export default defineConfig({
  test: {
    setupFiles: [
      'src/index.ts',
      'test/setup.js',
    ],
  },
});

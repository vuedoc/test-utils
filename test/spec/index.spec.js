import { join } from 'node:path';
import { expect, test } from 'vitest';

test('should success', async () => {
  const options = {
    filecontent: `
      <script setup>
        import { computed } from 'vue';

        const message = 'Hello, World!';
      </script>
    `,
  };

  await expect(options).toParseAs({
    errors: [],
    warnings: [],
    computed: [],
    data: [
      {
        kind: 'data',
        name: 'message',
        type: 'string',
        category: undefined,
        version: undefined,
        initialValue: '"Hello, World!"',
        keywords: [],
        visibility: 'public' },
    ],
    props: [],
    methods: [],
  });
});

test('should failed', async () => {
  const options = {
    filecontent: `
      <script setup>
        import { computed } from 'vue';

        const message = 'Hello, World!';
      </script>
    `,
  };

  await expect(options).not.toParseAs({
    errors: [],
    warnings: [],
    computed: [],
    data: [
      {
        kind: 'data',
        name: 'message',
        type: 'string',
        category: undefined,
        version: undefined,
        initialValue: 'Hello, World!',
        keywords: [],
        visibility: 'public' },
    ],
    props: [],
    methods: [],
  });
});

test('should raise missing file', async () => {
  const options = {
    filecontent: `
      <script setup>
        import { useRoute } from 'vue-routerx';

        const route = useRoute();
      </script>
    `,
  };

  await expect(options).toParseAs({
    errors: [
      "Cannot find module 'vue-routerx'. Make sure to define options.resolver",
    ],
    warnings: [],
    computed: [],
    data: [
      {
        kind: 'data',
        name: 'route',
        type: 'unknown',
        category: undefined,
        version: undefined,
        initialValue: 'useRoute()',
        keywords: [],
        visibility: 'public' },
    ],
    props: [],
    methods: [],
  });
});

test('should succedd to load deps using options.fakeNodeModulesPaths', async () => {
  const options = {
    filecontent: `
      <script setup>
        import { useStore } from 'vuex';

        const store = useStore();
      </script>
    `,
  };

  await expect(options).toParseAs({
    errors: [],
    warnings: [],
    computed: [],
    data: [
      {
        kind: 'data',
        name: 'store',
        type: 'unknown',
        category: undefined,
        version: undefined,
        initialValue: 'useStore()',
        keywords: [],
        visibility: 'public' },
    ],
    props: [],
    methods: [],
  }, {
    fakeNodeModulesPaths: [join(__dirname, '../fake_node_modules')],
  });
});

test('should succedd to load deps using globalThis.VUEDOC_FAKE_NODEMODULES_PATHS', async () => {
  const options = {
    filecontent: `
      <script setup>
        import { useRoute } from 'vue-router';

        const route = useRoute();
      </script>
    `,
  };

  await expect(options).toParseAs({
    errors: [],
    warnings: [],
    computed: [],
    data: [
      {
        kind: 'data',
        name: 'route',
        type: 'unknown',
        category: undefined,
        version: undefined,
        initialValue: 'useRoute()',
        keywords: [],
        visibility: 'public' },
    ],
    props: [],
    methods: [],
  });
});

test('should handle fatal event', async () => {
  const options = {
    filecontent: `
      <script setup>
        const !message = 'Hello, World!';
      </script>
    `,
  };

  await expect(options).toParseWithError('Unexpected token (2:14)');
});

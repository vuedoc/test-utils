# Vuedoc Test Utils

Component testing utils for Vuedoc.

[![npm](https://img.shields.io/npm/v/@vuedoc/test-utils.svg)](https://www.npmjs.com/package/@vuedoc/test-utils)
[![Build status](https://gitlab.com/vuedoc/test-utils/badges/main/pipeline.svg)](https://gitlab.com/vuedoc/test-utils/pipelines?ref=main)
[![Test coverage](https://gitlab.com/vuedoc/test-utils/badges/main/coverage.svg)](https://gitlab.com/vuedoc/test-utils/-/jobs)
[![Buy me a beer](https://img.shields.io/badge/Buy%20me-a%20beer-1f425f.svg)](https://www.buymeacoffee.com/demsking)

## Install

This package is [ESM only](https://gist.github.com/sindresorhus/a39789f98801d908bbc7ff3ecc99d99c)
: Node 16+ is needed to use it and it must be imported instead of required.

```sh
npm install --save-dev @vuedoc/test-utils
```

## Usage

Enable Vuedoc Test Utils on your `vitest.config.js` file:

```js
// vitest.config.js
import { defineConfig } from 'vitest/config';

export default defineConfig({
  test: {
    setupFiles: [
      '@vuedoc/test-utils',
    ],
  },
});
```

Then use the Vuedoc matchers `toParseAs` or `toParseWithError`:

**toParseAs**

```js
// my-test.spec.js
import { expect, test } from 'vitest';

test('should success', async () => {
  // your vuedoc parsing options
  const options = {
    filecontent: `
      <script setup>
        import { computed } from 'vue';

        const message = 'Hello, World!';
      </script>
    `,
  };

  await expect(options).toParseAs({
    errors: [],
    warnings: [],
    computed: [],
    data: [
      {
        kind: 'data',
        name: 'message',
        type: 'string',
        initialValue: '"Hello, World!"',
        keywords: [],
        visibility: 'public' },
    ],
    props: [],
    methods: [],
  });
});
```

**toParseWithError**

```js
// my-test.spec.js
import { expect, test } from 'vitest';

test('should success', async () => {
  // your vuedoc parsing options
  const options = {
    filecontent: `
      <script setup>
        import { computed } from 'vue';

        const !message = 'Unexpected token';
      </script>
    `,
  };

  await expect(options).toParseWithError('Unexpected token (2:14)');
});
```

**Vuedoc Matcher Interface**

```ts
interface VuedocMatchers<R = unknown> {
  toParseAs(expectedResult: ParsingResult, options?: ToParseAsOptions): R;
  toParseWithError(expectedErrorMessage: string, options?: ToParseAsOptions): R;
}

type ToParseAsOptions = {
  /**
   * By default `globalThis.VUEDOC_FAKE_NODEMODULES_PATHS`
   */
  fakeNodeModulesPaths: string[];
};
```

## Contribute

Please follow [CONTRIBUTING.md](https://gitlab.com/vuedoc/test-utils/blob/main/CONTRIBUTING.md).

## Versioning

Given a version number `MAJOR.MINOR.PATCH`, increment the:

- `MAJOR` version when you make incompatible API changes,
- `MINOR` version when you add functionality in a backwards-compatible manner,
  and
- `PATCH` version when you make backwards-compatible bug fixes.

Additional labels for pre-release and build metadata are available as extensions
to the `MAJOR.MINOR.PATCH` format.

See [SemVer.org](https://semver.org/) for more details.

## License

Under the MIT license.
See [LICENSE](https://gitlab.com/vuedoc/test-utils/blob/main/LICENSE) file for
more details.
